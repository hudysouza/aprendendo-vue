import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from './components/layouts/Dashboard.vue'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Person from './components/Person.vue'
import store from './store/store.js'

Vue.use(Router)

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: DashboardLayout,
      redirect: '/home',
      children: [
        { path: 'home', name: 'home', component: Home },
        { path: 'about', name: 'about', component: About },
        { path: 'people/:id', name: 'edit_people', component: Person },
        { path: 'people/', name: 'new_people', component: Person }
      ]
    },
    { path: '/login', name: 'login', component: Login },
    { path: '/register', name: 'register', component: Register }
  ]
})

router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = store.getters.isAuthenticated

  if (authRequired && !loggedIn) { return next('/login') } else { return next() }
})
