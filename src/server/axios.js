// import Vue from 'vue'
import axios from 'axios'
import store from '../store/store.js'
const api = axios.create({
  baseURL: 'http://192.168.99.100:3000/api/v1',
  json: true
})

export default {

  async execute (method, resource, data) {
    return api({
      method,
      url: resource,
      data,
      headers: {'Authorization': `Bearer ${store.state.user.token}`}
    }).then(req => {
      return req
    })
  }
}
