import Vue from 'vue'
import App from './App.vue'
import { router } from './router'
import store from './store/store'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: colors.deepPurple.lighten1,
    secondary: colors.green.lighten1,
    accent: colors.orange.darken4,
    error: colors.red.base,
    warning: colors.orange.darken1,
    info: colors.blue.base,
    success: colors.green.base
  }
})

Vue.use(VueAxios, axios)

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
