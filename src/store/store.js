import Vue from 'vue'
import Vuex from 'vuex'

import people from './modules/people'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    people,
    user
  }
})
