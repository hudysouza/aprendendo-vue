
import api from '@/server/axios.js'

export default {
  state: {
    people: []
  },
  mutations: {
    SET_PEOPLE (state, people) {
      state.people = people
    }
  },
  actions: {
    loadPeople ({ commit }) {
      api.execute('get', '/people')
        .then(r => {
          commit('SET_PEOPLE', r.data.people)
        })
    },
    async loadPerson (context, id) {
      return api.execute('get', `/people/${id}`)
    },
    async savePerson (context, person) {
      const id = person.id
      if (id) {
        return api.execute('put', `/people/${id}`, person)
      } else {
        return api.execute('post', '/people/', person)
      }
    },
    deletePerson (context, id) {
      return api.execute('delete', `/people/${id}`)
    },
    deletePhones (context, {personID, phones}) {
      phones.forEach(phone => {
        api.execute('delete', `/people/${personID}/phones`, phone)
      })
    }
  }
}
