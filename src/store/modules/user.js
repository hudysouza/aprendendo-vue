// https://auth0.com/blog/build-an-app-with-vuejs/

import axios from 'axios'
const BASE_URL = 'http://192.168.99.100:3000'

export default {
  getters: {
    isAuthenticated: state => !!state.token,
    authStatus: state => state.status,
    getProfile: state => state.profile,
    isProfileLoaded: state => !!state.profile.id
  },
  state: {
    token: localStorage.getItem('user-token') || '',
    profile: {},
    status: ''
  },
  mutations: {
    AUTH_REQUEST (state) {
      state.status = 'loading'
    },
    AUTH_SUCCESS (state, token) {
      state.status = 'success'
      state.token = token
    },
    AUTH_ERROR (state) {
      state.status = 'error'
    },
    AUTH_LOGOUT (state) {
      state.profile = {}
    },
    USER_REQUEST (state) {
      state.status = 'loading'
    },
    USER_SUCCESS (state, resp) {
      state.status = 'success'
      state.profile = resp
    },
    USER_ERROR (state) {
      state.status = 'error'
    }
  },
  actions: {
    loadUser ({commit, dispatch, state}) {
      return new Promise((resolve, reject) => {
        commit('USER_REQUEST')
        axios.get(`${BASE_URL}/api/v1/users/current`, {
          headers: {'Authorization': `Bearer ${state.token}`}
        }).then(ret => {
          commit('USER_SUCCESS', ret.data.user)
          resolve(ret)
        }).catch(err => {
          console.log(err)
          commit('USER_ERROR')
          dispatch('logout')
          reject(err)
        })
      })
    },
    login ({commit, dispatch}, auth) {
      return new Promise((resolve, reject) => {
        commit('AUTH_REQUEST')
        axios.post(
          `${BASE_URL}/api/v1/auth`,
          {auth: auth}
        ).then(ret => {
          const token = ret.data.jwt
          localStorage.setItem('user-token', token)
          commit('AUTH_SUCCESS', token)
          dispatch('loadUser', token)
          resolve(ret)
        }).catch(err => {
          commit('AUTH_ERROR')
          localStorage.removeItem('user-token')
          reject(err)
        })
      })
    },

    logout ({commit, dispatch}) {
      return new Promise((resolve, reject) => {
        commit('AUTH_LOGOUT')
        localStorage.removeItem('user-token')
        resolve()
      })
    },

    register ({commit, dispatch}, auth) {
      return new Promise((resolve, reject) => {
        commit('USER_REQUEST')
        axios.post(
          `${BASE_URL}/api/v1/users/create`,
          {user: auth}
        ).then(ret => {
          resolve(ret)
        }).catch(err => {
          commit('USER_ERROR')
          reject(err)
        })
      })
    }
  }
}
